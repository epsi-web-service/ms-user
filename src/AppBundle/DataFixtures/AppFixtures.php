<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFullname('Matteo Peronnet');
        $user->setUsername('matteo@epsi.fr');
        $user->setEmail('matteo@epsi.fr');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $manager->persist($user);

        $manager->flush();
    }
}
