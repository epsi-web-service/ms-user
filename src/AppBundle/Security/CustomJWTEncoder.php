<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use InvalidArgumentException;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Namshi\JOSE\SimpleJWS;

/**
 * JWTEncoder
 *
 * @author Dev Lexik <dev@lexik.fr>
 */
class CustomJWTEncoder implements JWTEncoderInterface
{
    const ALGORYTHM = 'HS256';

    /**
     * @var string
     */
    protected $privateKey;

    /**
     * @var string
     */
    protected $publicKey;

    /**
     * @var string
     */
    protected $passPhrase;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param string $privateKey
     * @param string $publicKey
     * @param string $passPhrase
     * @param EntityManager $entityManager
     */
    public function __construct($privateKey, $publicKey, $passPhrase, EntityManager $entityManager)
    {
        $this->privateKey = $privateKey;
        $this->publicKey  = $publicKey;
        $this->passPhrase = $passPhrase;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function encode(array $data)
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $data['username']]);
        $data['sub'] = $user->getId();
        $jws = new SimpleJWS(['alg' => self::ALGORYTHM, 'typ' => 'JWT']);
        $jws->setPayload($data);
        $jws->sign($this->privateKey);

        return $jws->getTokenString();
    }

    /**
     * {@inheritdoc}
     */
    public function decode($token)
    {
        try {
            /** @var SimpleJWS $jws */
            $jws = SimpleJWS::load($token);
        } catch (InvalidArgumentException $e) {
            return false;
        }

        if (!$jws->isValid($this->publicKey, self::ALGORYTHM)) {
            return false;
        }

        return $jws->getPayload();
    }

}
