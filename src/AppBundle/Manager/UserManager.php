<?php

declare(strict_types=1);

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class UserManager
{
    private $repository;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->repository = $manager->getRepository(User::class);
    }

    public function findOneBy(array $criteria, array $orderBy = null): ?User
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    public function find(int $id): ?User
    {
        return $this->repository->find($id);
    }
}
