#!/usr/bin/env bash
service mysql start
[ ! -f \"vendor/composer.phar\" ] && php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php && php composer-setup.php && mkdir -p vendor && mv composer.phar vendor/composer.phar && rm composer-setup.php
php vendor/composer.phar install --prefer-dist --no-progress
php bin/console --env=test doctrine:database:create
php bin/console --env=test doctrine:schema:update --force
