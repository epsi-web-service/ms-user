#!/usr/bin/env bash
cp app/config/parameters.yml.dist app/config/parameters.yml
service mysql start
Xvfb :99 -ac &
export DISPLAY=:99
nohup java -jar /selenium-server-standalone-2.53.0.jar 2> /dev/null > /dev/null &
php composer.phar install
php bin/console --env=test doctrine:database:create
php bin/console --env=test doctrine:schema:update --force
php bin/console --env=test doctrine:fixture:load -n -vv
php bin/console --env=test server:run 0.0.0.0:8080 -q &
php -d memory_limit=2048M bin/console --env=test cache:warmup --no-debug
sleep 10
