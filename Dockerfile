FROM troopers/docker-images:php7.2-apache


RUN pecl install pecl/apcu_bc-1.0.3 \
  && docker-php-ext-install pdo pdo_mysql intl \
  && echo "extension=apcu.so" > /usr/local/etc/php/conf.d/apcu.ini

ADD docker/vhost.conf /etc/apache2/sites-available/000-default.conf

RUN usermod -u ${DOCKER_USER_ID:-1000} www-data

COPY composer.json ./
COPY composer.lock ./
COPY app app/
COPY bin bin/
COPY src src/
COPY web web/
COPY docker docker/
