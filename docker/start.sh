#!/bin/sh

# Print commands and their arguments as they are executed
set -xe

# Detect the host IP
export DOCKER_BRIDGE_IP=$(ip ro | grep default | cut -d' ' -f 3)

mkdir -p var/cache var/log var/logs var/sessions var/fails var/cache/ci var/uploads/documents/ var/cache/prod var/cache/dev
chown -R www-data:www-data var

# Composer install
composer install --prefer-dist --no-scripts --no-progress --no-suggest --optimize-autoloader --classmap-authoritative
yes | cp -rf app/config/parameters.yml.prod app/config/parameters.yml

# Make rights on var folder
chmod -R 777 var/cache var/log var/sessions var/fails var/cache/ci

php bin/console doctrine:schema:update --force
php bin/console assets:install --env=prod


# Make file executable
chmod +x docker/start_safe_perms
# Start Apache with the right permissions after removing pre-existing PID file
rm -f /var/run/apache2/apache2.pid
exec docker/start_safe_perms -DFOREGROUND
