Feature: Log in an existing user

  Background:
    Given I add "Accept" header equal to "application/ld+json"
    And I add "Content-Type" header equal to "application/ld+json"

  Scenario: Log in existing user but don't add token header
    # Log in user
    When I send a "POST" request to "/api/login" with parameters:
      | key       | value |
      | _username | matteo@epsi.fr |
      | _password | password |
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "token" should exist
    And the JSON node "refresh_token" should exist
