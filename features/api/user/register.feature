Feature: Register an user

  Background:
    Given I add "Accept" header equal to "application/ld+json"
    And I add "Content-Type" header equal to "application/ld+json"

  Scenario: Register an user
    # Register
    When I send a "POST" request to "/api/register" with body:
    """
    {
      "email": "john@doe.com",
      "username": "john@doe.com",
      "plainPassword": "foobar"
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    And the JSON node "email" should exist
    And the JSON node "username" should exist
